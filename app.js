require('dotenv').config();

const {
  createBot,
  createProvider,
  createFlow,
  addKeyword,
} = require('@bot-whatsapp/bot');

const BaileysProvider = require('@bot-whatsapp/provider/venom');
//const BaileysProvider = require('@bot-whatsapp/provider/baileys');
const MockAdapter = require('@bot-whatsapp/database/json');

const flowSuporte = addKeyword(['ola', 'olá', 'oi']).addAnswer(
  'Olá! Tudo bem? Sou o Assistente Virtual da Delta Solar - Engenharia & Energias Renováveis. Estou aqui para ajuda-lo(a) em suas dúvidas. \n\nRealizamos todas as etapas do seu projeto, desde a concepção, como planejamento, execução e comissionamento com a rede de distribuição local. \n\nComo assistente virtual, sou capaz de responder somente uma pergunta por vez. No que posso ajudá-lo(a)? \n\n Para saber mais sobre os tópicos existentes digite *Ajuda*',
  {
    media:
      'https://yata.s3-object.locaweb.com.br/e44f3ce75fa0cb48fbea68f09d8e17ad66d53e63a2310b65ac259e8e14582982',
    capture: true,
  },
);

const flowLoc = addKeyword(['localizados?', 1]).addAnswer(
  'Rua Cidade de Rio Pardo, 91 Capão do Leão - RS - 500 metros da BR116, Jardim América',
);

const flowDemos = addKeyword(['demonstração', 'demonstrar', 2]).addAnswer(
  'Você pode acessar nosso monitoramento ao vivo de geração solar em nosso site: https://deltasolar.com.br/geracao-solar-ao-vivo',
);

const flowBlog = addKeyword(['blog', 3]).addAnswer(
  'Acesse nosso blog de Notícias, informações e curiosidades relacionadas com a Energia Sobloglar Fotovoltaica! https://microgeracaofv.wordpress.com ',
);
const flowOrcamento = addKeyword(['Orçamento', 'orçamento', 4]).addAnswer(
  'Receba um orçamento gratuito em nosso site acessando: https://deltasolar.com.br/solicitar-orcamento',
);
const flowBenefits = addKeyword(['benefícios', 'beneficios', 5]).addAnswer(
  'As empresas rapidamente estão percebendo os benefícios sociais e econômicos da adoção de energia solar (Pioneirismo e Competitividade)\n\n• As credenciais verdes são poderosos motores de decisões de compra dos consumidores \n• As empresas que se preocupam com sua comunidade e meio ambiente tendem a ter maior aceitação no mercado, funcionários mais engajados, e níveis mais elevados de moral',
);
const flowInfo = addKeyword(['informar mais', 6]).addAnswer(
  'Para mais informações acesse: www.deltasolar.com.br Email: contato@deltasolar.com.br  Facebook: www.facebook.com/deltasolarrs/',
);

const flowInstalation = addKeyword(['instalação', 7]).addAnswer(
  'A instalação do sistema de energia solar fotovoltaica dependerá das características do telhado ou do local onde será instalado. Em geral a instalação é realizada na telhado da edificação.\n\nMas também pode ser instalada em outras estruturas como garagens ou terrenos, desde que atendam aos requisitas de insolação e espaça necessário para as placas solares. \n\nA equipe técnica da Delta Solar fará uma análise do local para definir a melhor forma de Instalação. \n\nSe você quiser mais informações, por favar, solicite um orçamenta gratuito e sem compromisso em nosso site \nhttps://deltasolar.com.br/solicitar-orcamento',
);

const flowProtection = addKeyword(['proteção', 8]).addAnswer(
  'Nossos sistemas fotovoltaicos possuem proteções elétricas, como disjuntores. dispositivo de proteção contra surtos e aterramento a fim de garantir a segurança do equipamento e dos usuários.',
);

const flowWeb = addKeyword(['site', 9]).addAnswer(
  'Acesse nosso site\nhttps://deltasolar.com.br/',
);

const flowLifeTime = addKeyword(['Tempo dura', 'Vida útil', 10]).addAnswer(
  'Os sistemas fotovoltaicos conectados à rede elétrica tern uma vida útil de mais de 30 anos, enquanto que os sistemas isolados ou autônomos tem uma vida útil média de 20 anos. \n\nAlém disso, a manutenção do Sistema Fotovoltaico é quase Inexistente e geralmente apenas uma limpeza básica com água a cada meses é suficiente.',
  { sensitive: true },
);

const flowWrong = addKeyword([
  'irregularidades',
  'Irregularidades',
  '11',
]).addAnswer(
  'A Delta Solar oferece soluções e serviços nas áreas de eficiência energética e energias renováveis. \n\nEntre essas áreas, está incluída a realização de estudos de viabilidade técnica e econômica, a concepção e elaboração de projetos, além da regularização de sistemas fotovoltaicos junto à concessionária de energia. \n\nPorém, para saber se podemos ajudá-lo especificamente com o seu problema, seria necessário um orçamento personalizado. \n\nVocê pode solicitar um orçamento gratuito e sem compromisso em nossa Site:\nhttps://deltasolar.com.br/solicitar-orcamento',
  { sensitive: true },
);

const flowName = addKeyword(['seu nome', 'Seu nome', 'Seu Nome', 'seu Nome'])
  .addAnswer(
    'Eu sou o assistente virtual da Delta Solar, empresa de Engenharia & Energias Renováveis, Como posso ajudá-lo corn informações sabre energia solar?',
  )
  .addAnswer(
    'Estou aqui para responder perguntas e fornecer informações sobre Delta-Solar, seus serviços e soluções para energia solar, tem alguma pergunta sobre a empresa ou sobre energia solar em geral? Lembre-se de que só posso responder a uma pergunta por vez,',
  );

const flowAvaliation = addKeyword(['Avaliação', 'avaliação', '12']).addAnswer(
  'Sim. a Delta Solar oferece c serviço de visita técnica para avaliar a viabilidade da instalação de um sistema de energia solar fotovoltaica em sua residência. \n\nPor favar, solicite um orçamento gratuito e sem compromisso através do nosso site https://deltasolar.com.br/solicitar-orcamento ou entre em contato pelo Tel. WhatsApp: (53)92000-5258.',
  { sensitive: true },
);

const flowHelp = addKeyword(['Ajuda']).addAnswer(
  `Digite o numero equivalente ao tópico que deseja buscar ajuda. \n\n1 - Localização\n\n2 - Demonstração \n\n3 - Blog \n\n4 - Orçamento \n\n5 - Benefícios\n\n6 - Informações\n\n7 - Instalação\n\n8 - Proteção\n\n9 - Site\n\n10 - Vida Útil\n\n11 - Irregularidades\n\n12 - Avaliação`,
  { capture: true },
);

const flow = addKeyword(['']).addAnswer(' ');

const main = async () => {
  const adapterDB = new MockAdapter();

  const adapterFlow = createFlow([
    flowSuporte,
    flowLoc,
    flowDemos,
    flowBlog,
    flowOrcamento,
    flowBenefits,
    flowInfo,
    flowInstalation,
    flowProtection,
    flowWeb,
    flowLifeTime,
    flowWrong,
    flowName,
    flowAvaliation,
    flowHelp,
  ]);

  const adapterProvider = createProvider(BaileysProvider);

  createBot({
    flow: adapterFlow,
    provider: adapterProvider,
    database: adapterDB,
  });
};

main();
